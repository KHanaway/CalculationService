﻿using System.Collections.Specialized;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolCalculation.Client
{
    public class HttpClientWrapper : IHttpClientWrapper
    {
        private readonly HttpClient _client;

        public HttpClientWrapper(HttpClient client)
        {
            _client = client;
        }

        public async Task<string> GetRequestAsync(NameValueCollection queryParams)
        {
            var response = await _client.GetAsync($"?{queryParams}");
            return await response?.Content?.ReadAsStringAsync();
        }
    }
}
