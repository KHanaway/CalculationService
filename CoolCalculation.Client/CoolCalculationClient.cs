﻿using System;
using System.Threading.Tasks;
using System.Web;

namespace CoolCalculation.Client
{
    public class CoolCalculationClient : ICoolCalculationClient
    {
        private readonly IHttpClientWrapper _client;

        public CoolCalculationClient(IHttpClientWrapper client)
        {
            _client = client;
        }

        public async Task<decimal> EvaluateAsync(string expr)
        {
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["expr"] = expr;

            var response = await _client.GetRequestAsync(query);
            
            if (!decimal.TryParse(response, out var result))
            {
                throw new Exception(response);
            }

            return result;
        }
    }
}
