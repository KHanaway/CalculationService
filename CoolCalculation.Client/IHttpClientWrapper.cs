﻿using System.Collections.Specialized;
using System.Threading.Tasks;

namespace CoolCalculation.Client
{
    public interface IHttpClientWrapper
    {
        Task<string> GetRequestAsync(NameValueCollection queryParams);
    }
}
