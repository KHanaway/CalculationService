﻿using System.Threading.Tasks;

namespace CoolCalculation.Client
{
    public interface ICoolCalculationClient
    {
        // Legacy method. Should not be called for new operations
        Task<decimal> EvaluateAsync(string expr);
    }
}
