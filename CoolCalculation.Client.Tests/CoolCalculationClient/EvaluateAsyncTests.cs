﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace CoolCalculation.Client.Tests.CoolCalculationClient
{
    [TestFixture]
    internal class EvaluateAsyncTests : CoolCalculationClientTestsBase
    {
        [Test]
        public async Task EvaluateAsyncTests_ReturnsResult_WhenClientReturnsDecimalValue()
        {
            // Arrange
            var expr = "3.5*3";
            var expectedResult = 10.5m;

            MockHttpClientWrapper.Setup(x => x.GetRequestAsync(It.Is<NameValueCollection>(x => x.ToString() == $"expr={expr}")))
                                 .ReturnsAsync(expectedResult.ToString());

            // Act
            var result = await CoolCalculationClient.EvaluateAsync(expr);

            // Asset
            result.Should().Be(expectedResult);
        }

        [Test]
        public void EvaluateAsyncTests_ThrowsException_WhenClientReturnsNonDecimalValue()
        {
            // Arrange
            var expr = "3.5*3";

            MockHttpClientWrapper.Setup(x => x.GetRequestAsync(It.Is<NameValueCollection>(x => x.ToString() == $"expr={expr}")))
                                 .ReturnsAsync("Invalid Response");

            // Act
            Func<Task> action = async () => await CoolCalculationClient.EvaluateAsync(expr);

            // Asset
            action.Should().Throw<Exception>();
        }
    }
}
