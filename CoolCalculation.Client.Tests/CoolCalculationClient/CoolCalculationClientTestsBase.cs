﻿using Moq;
using NUnit.Framework;

namespace CoolCalculation.Client.Tests.CoolCalculationClient
{
    internal abstract class CoolCalculationClientTestsBase
    {
        protected Mock<IHttpClientWrapper> MockHttpClientWrapper { get; set; }

        protected Client.CoolCalculationClient CoolCalculationClient
        {
            get
            {
                return new Client.CoolCalculationClient(MockHttpClientWrapper.Object);
            }
        }

        [SetUp]
        public void Setup()
        {
            MockHttpClientWrapper = new Mock<IHttpClientWrapper>(MockBehavior.Strict);
        }

        [TearDown]
        public void TearDown()
        {
            MockHttpClientWrapper.VerifyAll();
        }
    }
}
