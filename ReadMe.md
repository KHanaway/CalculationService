# Calculation Service
A microservice that covers the requirements of the EQ Paymaster - Life and Pensions Project. 
A swagger client is included in this source to test this code. Simply run the Web project under IIS Express
and load the https://localhost:44315/swagger url to begin testing.

This projects supports all the functionality specified in the document but also includes additional 
support for expressions containing parenthesis and negative divison and multiplication.

The code will call https://api.mathjs.org/#get if it can't parse an expression. To check if the internal evaluation works
just comment out the second try catch block in the CalculationController otherwise it will be difficult to tell if
the internal mechanism failed to handle the expression.

Author: Keith Hanaway
