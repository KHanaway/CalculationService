﻿namespace Domain.Abstractions.Evaluators
{
    public interface IExpressionEvaluator
    {
        decimal Evaluate(string expression);
    }
}
