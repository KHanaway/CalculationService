﻿using System;

namespace Domain.Abstractions.Exceptions
{
    public class EvaluateExpressionException : Exception
    {
        public EvaluateExpressionException()
            : base()
        {
        }

        public EvaluateExpressionException(string message)
            : base(message)
        {
        }

        public EvaluateExpressionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
