﻿using CoolCalculation.Client;
using Domain.Abstractions.Evaluators;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace Web.Tests.Controllers.CalculationController
{
    internal abstract class CalculationControllerTestsBase
    {

        protected Mock<IExpressionEvaluator> MockExpressionEvaluator { get; set; }
        protected Mock<ICoolCalculationClient> MockCoolCalculationClient { get; set; }
        protected Mock<ILogger<Web.Controllers.CalculationController>> MockLogger { get; set; }

        protected Web.Controllers.CalculationController CalculationController
        {
            get
            {
                var context = new ControllerContext { HttpContext = new DefaultHttpContext() };
                context.HttpContext.Request.Scheme = "https";
                context.HttpContext.Request.Host = new HostString("www.iqvia.com", 443);
                context.HttpContext.Request.PathBase = string.Empty;
                context.HttpContext.Request.QueryString = new QueryString(string.Empty);
                context.HttpContext.Request.Path = "/calculation";

                return new Web.Controllers.CalculationController(
                                                                 MockExpressionEvaluator.Object,
                                                                 MockCoolCalculationClient.Object,
                                                                 MockLogger.Object)
                {
                    ControllerContext = context
                };
            }
        }

        [SetUp]
        public void Setup()
        {
            MockExpressionEvaluator = new Mock<IExpressionEvaluator>(MockBehavior.Strict);
            MockCoolCalculationClient = new Mock<ICoolCalculationClient>(MockBehavior.Strict);            
            MockLogger = new Mock<ILogger<Web.Controllers.CalculationController>>(MockBehavior.Loose);
        }

        [TearDown]
        public void TearDown()
        {
            MockExpressionEvaluator.VerifyAll();
            MockCoolCalculationClient.VerifyAll();
            MockLogger.VerifyAll();
        }
    }
}
