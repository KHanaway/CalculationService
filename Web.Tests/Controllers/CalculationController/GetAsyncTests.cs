﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Web.Tests.Controllers.CalculationController
{
    [TestFixture]
    internal class GetAsyncTests : CalculationControllerTestsBase
    {
        [Test]
        public async Task GetAsync_ReturnsCalculationResult_WhenSuccess()
        {
            // Arrange
            var expr = "10+6";
            var expectedResponse = 16m;
            
            MockExpressionEvaluator.Setup(x => x.Evaluate(expr)).Returns(expectedResponse);

            // Act
            var result = await CalculationController.GetAsync(expr);

            // Assert
            result.Should().BeOfType<OkObjectResult>();
            ((OkObjectResult)result).StatusCode.Should().Be((int)HttpStatusCode.OK);
            ((OkObjectResult)result).Value.Should().Be(expectedResponse);
        }

        [Test]
        public async Task GetAsync_CallsCoolCalculationClient_WhenInternalServiceThrowsException()
        {
            // Arrange
            var expr = "10+6";
            var expectedResponse = 16m;

            MockExpressionEvaluator.Setup(x => x.Evaluate(expr)).Throws(new Exception());
            MockCoolCalculationClient.Setup(x => x.EvaluateAsync(expr)).ReturnsAsync(expectedResponse);

            // Act
            var result = await CalculationController.GetAsync(expr);

            // Assert
            result.Should().BeOfType<OkObjectResult>();
            ((OkObjectResult)result).StatusCode.Should().Be((int)HttpStatusCode.OK);
            ((OkObjectResult)result).Value.Should().Be(expectedResponse);
        }

        [Test]
        public async Task GetAsync_ReturnsBadRequest_WhenCoolCalculationClientThrowsException()
        {
            // Arrange
            var expr = "10+6";
            var error = "Bad request";

            MockExpressionEvaluator.Setup(x => x.Evaluate(expr)).Throws(new Exception());
            MockCoolCalculationClient.Setup(x => x.EvaluateAsync(expr)).ThrowsAsync(new Exception(error));

            // Act
            var result = await CalculationController.GetAsync(expr);

            // Assert
            result.Should().BeOfType<BadRequestObjectResult>();
            ((BadRequestObjectResult)result).StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            ((BadRequestObjectResult)result).Value.Should().Be(error);
        }
    }
}
