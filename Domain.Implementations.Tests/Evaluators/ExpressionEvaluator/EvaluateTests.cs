﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Implementations.Tests.Evaluators.ExpressionEvaluator
{
    [TestFixture]
    internal class EvaluateTests : ExpressionEvaluatorTestsBase
    {
        [Test]
        public void Evaluate_SupportsSubtraction()
        {
            // Arrange
            var expr = "6.5-4.25";
            var expectedResult = 2.25m;

            MockExpressionParser.Setup(x => x.GetExpressionsWithinParenthesis(expr)).Returns(new List<string>());
            MockExpressionParser.Setup(x => x.GetIndividualOperatorExpressions(It.Is<char[]>(x => x.SequenceEqual(_operators)), '-', expr))
                                .Returns(new List<string>() { "6.5", "-4.25" });
            MockCalculationEngine.Setup(x => x.Compute('+', 6.5m, -4.25m)).Returns(expectedResult);

            // Act
            var result = ExpressionEvaluator.Evaluate(expr);

            // Assert
            result.Should().Be(expectedResult);
        }

        [Test]
        public void Evaluate_SupportsAddition()
        {
            // Arrange
            var expr = "5.5+1.5";
            var expectedResult = 7m;

            MockExpressionParser.Setup(x => x.GetExpressionsWithinParenthesis(expr)).Returns(new List<string>());
            MockExpressionParser.SetupSequence(x => x.GetIndividualOperatorExpressions(
                                                                                       It.Is<char[]>(x => x.SequenceEqual(_operators)), 
                                                                                       It.Is<char>(x => _operators.Any(y => y == x)), 
                                                                                       It.IsAny<string>()))
                                .Returns(new List<string>() { expr })
                                .Returns(new List<string>() { "5.5", "1.5" });

            MockCalculationEngine.Setup(x => x.Compute('+', 5.5m, 1.5m)).Returns(expectedResult);

            // Act
            var result = ExpressionEvaluator.Evaluate(expr);

            // Assert
            result.Should().Be(expectedResult);
        }


        [Test]
        public void Evaluate_SupportsMultiplication()
        {
            // Arrange
            var expr = "6.5*4.25";
            var expectedResult = 27.625m;

            MockExpressionParser.Setup(x => x.GetExpressionsWithinParenthesis(expr)).Returns(new List<string>());
            MockExpressionParser.SetupSequence(x => x.GetIndividualOperatorExpressions(
                                                                                       It.Is<char[]>(x => x.SequenceEqual(_operators)),
                                                                                       It.Is<char>(x => _operators.Any(y => y == x)),
                                                                                       It.IsAny<string>()))
                                .Returns(new List<string>() { expr })
                                .Returns(new List<string>() { expr })
                                .Returns(new List<string>() { "6.5", "4.25" });

            MockCalculationEngine.Setup(x => x.Compute('*', 6.5m, 4.25m)).Returns(expectedResult);

            // Act
            var result = ExpressionEvaluator.Evaluate(expr);

            // Assert
            result.Should().Be(expectedResult);
        }

        [Test]
        public void Evaluate_SupportsDivision()
        {
            // Arrange
            var expr = "7/3.5";
            var expectedResult = 2m;

            MockExpressionParser.Setup(x => x.GetExpressionsWithinParenthesis(expr)).Returns(new List<string>());
            MockExpressionParser.SetupSequence(x => x.GetIndividualOperatorExpressions(
                                                                                       It.Is<char[]>(x => x.SequenceEqual(_operators)),
                                                                                       It.Is<char>(x => _operators.Any(y => y == x)),
                                                                                       It.IsAny<string>()))
                                .Returns(new List<string>() { expr })
                                .Returns(new List<string>() { expr })
                                .Returns(new List<string>() { expr })
                                .Returns(new List<string>() { "6.5", "4.25" });

            MockCalculationEngine.Setup(x => x.Compute('/', 6.5m, 4.25m)).Returns(expectedResult);

            // Act
            var result = ExpressionEvaluator.Evaluate(expr);

            // Assert
            result.Should().Be(expectedResult);
        }

        [Test]
        public void Evaluate_SupportsChainedOperations()
        {
            // Arrange
            var expr = "7/3.5+3.5*10-6";

            MockExpressionParser.Setup(x => x.GetExpressionsWithinParenthesis(expr)).Returns(new List<string>());

            MockExpressionParser.SetupSequence(x => x.GetIndividualOperatorExpressions(
                                                                           It.Is<char[]>(x => x.SequenceEqual(_operators)),
                                                                           It.Is<char>(x => _operators.Any(y => y == x)),
                                                                           It.IsAny<string>()))
                    .Returns(new List<string>() { "7/3.5+3.5*10", "-6" })
                    .Returns(new List<string>() { "7/3.5", "3.5*10" })
                    .Returns(new List<string>() { "7/3.5" })
                    .Returns(new List<string>() { "7", "3.5" })
                    .Returns(new List<string>() { "3.5", "10" });

            MockCalculationEngine.Setup(x => x.Compute('/', 7m, 3.5m)).Returns(2m);
            MockCalculationEngine.Setup(x => x.Compute('*', 3.5m, 10m)).Returns(35m);
            MockCalculationEngine.Setup(x => x.Compute('+', 2m, 35m)).Returns(37m);
            MockCalculationEngine.Setup(x => x.Compute('+', 37m, -6m)).Returns(31m);

            // Act
            var result = ExpressionEvaluator.Evaluate(expr);

            // Assert
            result.Should().Be(31m);
        }

        [Test]
        public void Evaluate_SupportsChainedOperationsWithNegativeOperands()
        {
            // Arrange
            var expr = "7/-3.5+3.5*6-10";

            MockExpressionParser.Setup(x => x.GetExpressionsWithinParenthesis(expr)).Returns(new List<string>());

            MockExpressionParser.SetupSequence(x => x.GetIndividualOperatorExpressions(
                                                                           It.Is<char[]>(x => x.SequenceEqual(_operators)),
                                                                           It.Is<char>(x => _operators.Any(y => y == x)),
                                                                           It.IsAny<string>()))
                    .Returns(new List<string>() { "7/-3.5+3.5*6", "-10" })
                    .Returns(new List<string>() { "7/-3.5", "3.5*6" })
                    .Returns(new List<string>() { "7/-3.5" })
                    .Returns(new List<string>() { "7","-3.5" })
                    .Returns(new List<string>() { "3.5", "6" })
                    .Returns(new List<string>() { "3.5", "10" });

            MockCalculationEngine.Setup(x => x.Compute('/', 7m, -3.5m)).Returns(-2m);
            MockCalculationEngine.Setup(x => x.Compute('*', 3.5m, 6m)).Returns(21m);
            MockCalculationEngine.Setup(x => x.Compute('+', -2m, 21m)).Returns(19m);
            MockCalculationEngine.Setup(x => x.Compute('+', 19m, -10m)).Returns(9m);

            // Act
            var result = ExpressionEvaluator.Evaluate(expr);

            // Assert
            result.Should().Be(9m);
        }

        [Test]
        public void Evaluate_SupportsOperationsWithParenthesis()
        {
            // Arrange
            var expr = "(3.5+3.5)*(10-6)";

            MockExpressionParser.Setup(x => x.GetExpressionsWithinParenthesis(expr)).Returns(new List<string>
            {
                "3.5+3.5",
                "10-6"
            });

            MockExpressionParser.Setup(x => x.GetExpressionsWithinParenthesis("7*4")).Returns(new List<string>());

            MockExpressionParser.SetupSequence(x => x.GetIndividualOperatorExpressions(
                                                                           It.Is<char[]>(x => x.SequenceEqual(_operators)),
                                                                           It.Is<char>(x => _operators.Any(y => y == x)),
                                                                           It.IsAny<string>()))
                    .Returns(new List<string>() { "3.5", "3.5" })
                    .Returns(new List<string>() { "10", "-6" })
                    .Returns(new List<string>() { "7.0*4" })
                    .Returns(new List<string>() { "7.0*4" })
                    .Returns(new List<string>() { "7.0", "4" });

            MockCalculationEngine.Setup(x => x.Compute('+', 3.5m, 3.5m)).Returns(7m);
            MockCalculationEngine.Setup(x => x.Compute('+', 10m, -6m)).Returns(4m);
            MockCalculationEngine.Setup(x => x.Compute('*', 7m, 4m)).Returns(28m);     
            

            // Act
            var result = ExpressionEvaluator.Evaluate(expr);

            // Assert
            result.Should().Be(28m);
        }
    }
}
