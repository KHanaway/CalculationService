﻿using Domain.Implementations.Parsers;
using Domain.Implementations.Utilities;
using Moq;
using NUnit.Framework;

namespace Domain.Implementations.Tests.Evaluators.ExpressionEvaluator
{
    internal abstract class ExpressionEvaluatorTestsBase
    {
        protected readonly char[] _operators = new char[] { '-', '+', '*', '/' };

        protected Mock<IExpressionParser> MockExpressionParser { get; set; }

        protected Mock<ICalculator> MockCalculationEngine { get; set; }

        protected Implementations.Evaluators.ExpressionEvaluator ExpressionEvaluator
        {
            get
            {
                return new Implementations.Evaluators.ExpressionEvaluator(MockExpressionParser.Object, MockCalculationEngine.Object);
            }
        }

        [SetUp]
        public void Setup()
        {
            MockExpressionParser = new Mock<IExpressionParser>(MockBehavior.Strict);
            MockCalculationEngine = new Mock<ICalculator>(MockBehavior.Strict);
        }

        [TearDown]
        public void TearDown()
        {
            MockExpressionParser.VerifyAll();
            MockCalculationEngine.VerifyAll();
        }
    }
}
