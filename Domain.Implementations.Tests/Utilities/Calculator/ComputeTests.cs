﻿using FluentAssertions;
using NUnit.Framework;
using System;

namespace Domain.Implementations.Tests.Utilities.Calculator
{
    [TestFixture]
    internal class ComputeTests : CalculatorTestsBase
    {
        [Test]
        public void Compute_SupportsSubtraction()
        {
            // Arrange

            // Act
            var result = Calculator.Compute('-', 5.75m, 1.5m);

            // Asset
            result.Should().Be(4.25m);
        }

        [Test]
        public void Compute_SupportsAddition()
        {
            // Arrange

            // Act
            var result = Calculator.Compute('+', 5.75m, 1.5m);

            // Asset
            result.Should().Be(7.25m);
        }

        [Test]
        public void Compute_SupportsMultiplication()
        {
            // Arrange

            // Act
            var result = Calculator.Compute('*', 10m, 2.5m);

            // Asset
            result.Should().Be(25m);
        }

        [Test]
        public void Compute_SupportsDivision()
        {
            // Arrange

            // Act
            var result = Calculator.Compute('/', 3.5m, 1.75m);

            // Asset
            result.Should().Be(2m);
        }

        [Test]
        public void Compute_ThrowsArgumentException_ForUnsupportedOperators()
        {
            // Arrange

            // Act
            Action action = () => Calculator.Compute('%', 3.5m, 1.75m);

            // Asset
            action.Should().Throw<ArgumentException>();
        }
    }
}
