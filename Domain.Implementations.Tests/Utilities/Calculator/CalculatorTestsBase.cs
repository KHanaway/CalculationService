﻿namespace Domain.Implementations.Tests.Utilities.Calculator
{
    internal abstract class CalculatorTestsBase
    {
        protected Implementations.Utilities.Calculator Calculator
        {
            get
            {
                return new Implementations.Utilities.Calculator();
            }
        }
    }
}
