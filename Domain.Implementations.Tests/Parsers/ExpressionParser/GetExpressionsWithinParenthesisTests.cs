﻿using FluentAssertions;
using NUnit.Framework;

namespace Domain.Implementations.Tests.Parsers.ExpressionParser
{
    [TestFixture]
    internal class GetExpressionsWithinParenthesisTests : ExpressionParserBaseTests
    {
        [Test]
        public void GetExpressionsWithinParenthesis_ReturnsMatchingResults_WhenExpressionHasParenthesis()
        {
            // Arrange

            // Act
            var result = ExpressionParser.GetExpressionsWithinParenthesis("(3-5)+(8*2)+9+(5-1)");

            // Assert
            result.Should().BeEquivalentTo(new[] { "3-5", "8*2", "5-1" });
        }

        [Test]
        public void GetExpressionsWithinParenthesis_ReturnsEmptyList_WhenExpressionHasNoParenthesis()
        {
            // Arrange

            // Act
            var result = ExpressionParser.GetExpressionsWithinParenthesis("3-5+8*2+9+5-1");

            // Assert
            result.Should().BeEmpty();
        }
    }
}
