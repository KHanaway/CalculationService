﻿using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;

namespace Domain.Implementations.Tests.Parsers.ExpressionParser
{
    [TestFixture]
    internal class GetIndividualOperatorExpressionsTests : ExpressionParserBaseTests
    {
        [Test]
        public void GetIndividualOperatorExpressions_SplitsString_WhenCurrentOperatorNotMinus()
        {
            // Arrange
            var expression = "-3-5+8*-2-1/-1";

            // Act
            var result = ExpressionParser.GetIndividualOperatorExpressions(_operators, '+', expression);

            // Assert
            result.Should().BeEquivalentTo(expression.Split("+"));
        }

        [Test]
        public void GetIndividualOperatorExpressions_ReplacesSubtractionExpressionsWithNegativeAdditions_WhenCurrentOperatorIsMinus()
        {
            // Arrange
            var expression = "-3-5+8*-2-1/-1";

            // Act
            var result = ExpressionParser.GetIndividualOperatorExpressions(_operators, '-', expression);

            // Assert
            result.Should().BeEquivalentTo(new List<string> { "-3",  "-5+8*-2", "-1/-1" });
        }
    }
}
