﻿namespace Domain.Implementations.Tests.Parsers.ExpressionParser
{
    internal abstract class ExpressionParserBaseTests 
    {
        protected readonly char[] _operators = new char[] { '-', '+', '*', '/' };

        protected Implementations.Parsers.ExpressionParser ExpressionParser
        {
            get
            {
                return new Implementations.Parsers.ExpressionParser();
            }
        }
    }
}
