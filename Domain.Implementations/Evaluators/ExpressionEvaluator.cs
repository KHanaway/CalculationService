﻿using Domain.Abstractions.Evaluators;
using Domain.Abstractions.Exceptions;
using Domain.Implementations.Extensions;
using Domain.Implementations.Parsers;
using Domain.Implementations.Utilities;
using System.Linq;

namespace Domain.Implementations.Evaluators
{
    public class ExpressionEvaluator : IExpressionEvaluator
    {
        private readonly char[] _supportedOperators = new char[] { '-', '+', '*', '/' };

        private readonly IExpressionParser _expressionParser;
        private readonly ICalculator _calculator;

        public ExpressionEvaluator(IExpressionParser expressionParser, ICalculator calculator)
        {
            _expressionParser = expressionParser;
            _calculator = calculator;
        }

        public decimal Evaluate(string expr)
        {
            var expression = EvaluateExpressionsWithinParenthesis(expr);
            return Evaluate(expression, 0);
        }

        private string EvaluateExpressionsWithinParenthesis(string expr)
        {
            var parsedExpression = expr;

            var expressionsWithinParenthesis = _expressionParser.GetExpressionsWithinParenthesis(parsedExpression);

            while (!expressionsWithinParenthesis.IsNullOrEmpty())
            {
                expressionsWithinParenthesis.ForEach(x =>
                {
                    var evaluatedExpression = Evaluate(x, 0).ToString();
                    parsedExpression = parsedExpression.Replace($"({x})", evaluatedExpression);
                });

                expressionsWithinParenthesis = _expressionParser.GetExpressionsWithinParenthesis(parsedExpression);
            }

            return parsedExpression.Replace("-+", "-");
        }

        private decimal Evaluate(string expr, int index)
        {
            decimal? result = null;
            var currentOperator = _supportedOperators[index];

            _expressionParser.GetIndividualOperatorExpressions(_supportedOperators, currentOperator, expr).ForEach(operand =>
            {
                if (!decimal.TryParse(operand, out var number))
                {
                    if (index + 1 >= _supportedOperators.Count())
                    {
                        throw new EvaluateExpressionException("The expression passed to the service contained invalid characters.");
                    }

                    number = Evaluate(operand, index + 1);
                }

                // Just treat subtraction as adding a negative number
                var arithmeticOperator = currentOperator == '-' ?  '+' : currentOperator;
                result = (result.HasValue) ? _calculator.Compute(arithmeticOperator , result.Value, number) : number;
            });

            return result.Value;
        }
    }
}
