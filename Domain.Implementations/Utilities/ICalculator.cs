﻿namespace Domain.Implementations.Utilities
{
    public interface ICalculator
    {
        decimal Compute(char arithmeticOperator, decimal operand1, decimal operand2);
    }
}
