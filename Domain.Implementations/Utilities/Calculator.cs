﻿using System;

namespace Domain.Implementations.Utilities
{
    public class Calculator : ICalculator
    {
        public decimal Compute(char arithmeticOperator, decimal operand1, decimal operand2)
        {
            decimal result;

            switch (arithmeticOperator)
            {
                case '/':
                    result = operand1 / operand2;
                    break;
                case '*':
                    result = operand1 * operand2;
                    break;
                case '+':
                    result = operand1 + operand2;
                    break;
                case '-':
                    result = operand1 - operand2;
                    break;
                default:
                    throw new ArgumentException($"Invalid operator pass to method: {arithmeticOperator}");

            }

            return result;
        }
    }
}
