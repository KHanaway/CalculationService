﻿using System.Collections.Generic;

namespace Domain.Implementations.Parsers
{
    public interface IExpressionParser
    {
        List<string> GetExpressionsWithinParenthesis(string expr);

        List<string> GetIndividualOperatorExpressions(
                                                      char[] supportedOperators,
                                                      char currentOperator,
                                                      string expr);
    }
}
