﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Domain.Implementations.Parsers
{
    public class ExpressionParser : IExpressionParser
    {
        private readonly Regex _parenthesisRegex = new Regex(@"\([\d+*.\-\/]*\)");

        public List<string> GetExpressionsWithinParenthesis(string expr)
        {
            var matches = _parenthesisRegex.Matches(expr);
            var expressions = matches.Select(x => x.Value.Trim('(', ')'));

            return expressions.Distinct().ToList();
        }

        public List<string> GetIndividualOperatorExpressions(
                                                             char[] supportedOperators,
                                                             char currentOperator,
                                                             string expr)
        {
            var results = expr.Split(currentOperator, System.StringSplitOptions.TrimEntries).ToList();

            // The negative operator needs some additional parsing as it can be
            // used both to respresent a subtraction or just a negative number
            if (currentOperator == '-')
            {
                var otherOperators = supportedOperators.Where(x => x != currentOperator).ToArray();

                var parsedResults = new List<string>();

                for (int i = 0; i < results.Count();  ++i)
                {
                    var e = results.ElementAt(i);

                    if (i == 0 && string.IsNullOrEmpty(e))
                    {
                        e = $"{e}-{results.ElementAt(++i)}";                        
                    }
                    else if (i > 0) 
                    {
                        e = $"-{e}";
                    }

                    while (otherOperators.Any(x => e.EndsWith(x) && ++i < results.Count()))
                    {
                        e = $"{e}-{results.ElementAt(i)}";
                    }
                    
                    parsedResults.Add(e);
                }

                results = parsedResults; 
            }

            return results.Where(x => !string.IsNullOrWhiteSpace(x)).ToList(); 
        }
    }
}
