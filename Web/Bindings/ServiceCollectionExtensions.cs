﻿using CoolCalculation.Client;
using Domain.Abstractions.Evaluators;
using Domain.Implementations.Evaluators;
using Domain.Implementations.Parsers;
using Domain.Implementations.Utilities;
using Microsoft.Extensions.DependencyInjection;

namespace Web.Bindings
{
    public static class ServiceCollectionExtensions
    {
        public static void BindDependencies(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IHttpClientWrapper, HttpClientWrapper>();
            serviceCollection.AddSingleton<ICoolCalculationClient, CoolCalculationClient>();
            serviceCollection.AddSingleton<ICalculator, Calculator>();
            serviceCollection.AddSingleton<IExpressionEvaluator, ExpressionEvaluator>();
            serviceCollection.AddSingleton<IExpressionParser, ExpressionParser>();
        }
    }
}
