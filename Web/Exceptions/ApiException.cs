﻿using System;
using System.Net;

namespace Web.Exceptions
{
    public class ApiException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }

        public ApiException(string message, Exception ex, HttpStatusCode statusCode)
            : base(message, ex)
        {
            StatusCode = statusCode;
        }
    }
}
