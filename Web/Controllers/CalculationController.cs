﻿using CoolCalculation.Client;
using Domain.Abstractions.Evaluators;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CalculationController : ControllerBase
    {
        private readonly IExpressionEvaluator _expressionEvaluator;
        private readonly ICoolCalculationClient _coolCalculationClient;
        private readonly ILogger<CalculationController> _logger;

        public CalculationController(
                                     IExpressionEvaluator expressionEvaluator,
                                     ICoolCalculationClient coolCalculationClient,
                                     ILogger<CalculationController> logger)
        {
            _expressionEvaluator = expressionEvaluator;
            _coolCalculationClient = coolCalculationClient;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAsync(string expr)
        {
            _logger.LogInformation(nameof(GetAsync), expr);

            try
            {
                return Ok(_expressionEvaluator.Evaluate(expr));
            }
            catch (Exception ex)
            {
                _logger.LogError(nameof(GetAsync), ex);
            }

            try
            {
                return Ok(await _coolCalculationClient.EvaluateAsync(expr));
            }
            catch (Exception ex)
            {
                _logger.LogError(nameof(GetAsync), ex);
                return BadRequest(ex.Message);
            }
        }
    }
}
